from setuptools import setup, find_packages


setup(
    name='myrm',
    packages=find_packages(),
    test_suite="test_delete_for_trash",
    entry_points={
        'console_scripts':
            ['myrm = work.core:coreFunc']
    }
)
