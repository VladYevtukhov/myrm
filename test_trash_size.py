from unittest import TestCase
from work.policies import trashSize
import os
import tempfile
import shutil


class TrashSizeTestCase(TestCase):
    def setUp(self):
        self.path_to_testing = tempfile.mkdtemp()
        if not os.path.exists(os.path.join(self.path_to_testing, "test_trash/files/")):
            os.makedirs(os.path.join(self.path_to_testing, "test_trash/files/"))
        if not os.path.exists(os.path.join(self.path_to_testing, "test_trash/info/")):
            os.makedirs(os.path.join(self.path_to_testing, "test_trash/info/"))

    def tearDown(self):
        shutil.rmtree(self.path_to_testing)

    def test_trashSize(self):
        self.assertEqual(trashSize(3, os.path.join(self.path_to_testing, "test_trash/info/")), True)

    def test_trashSize2(self):
        self.assertEqual(trashSize(0, os.path.join(self.path_to_testing, "test_trash/info/")), False)

    def test_trashSize3(self):
        self.assertEqual(trashSize(0, os.path.join(self.path_to_testing, "test_trash/2/")), False)

    def test_trashSize4(self):
        self.assertEqual(trashSize(3, os.path.join(self.path_to_testing, "test_trash/2/")), False)
