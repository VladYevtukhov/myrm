from unittest import TestCase
from work.politics import getMemory
import os
import tempfile
import shutil


class GetMemoryTestCase(TestCase):
    def setUp(self):
        self.path_to_testing = tempfile.mkdtemp()
        if not os.path.exists(os.path.join(self.path_to_testing, "test_trash/files/")):
            os.makedirs(os.path.join(self.path_to_testing, "test_trash/files/"))
        if not os.path.exists(os.path.join(self.path_to_testing, "test_trash/info/")):
            os.makedirs(os.path.join(self.path_to_testing, "test_trash/info/"))

    def tearDown(self):
        shutil.rmtree(self.path_to_testing)

    def test_getMemory(self):
        self.assertEqual(getMemory(os.path.join(self.path_to_testing, "test_trash/info/")), 0)

    def test_getMemory2(self):
        with open(os.path.join(self.path_to_testing, "file"), 'w') as trash_file:
            trash_file.writelines(["[Trash Info\n]", "test"])
        self.assertNotEqual(getMemory(os.path.join(self.path_to_testing, "file")), 0)

    def test_getMemory3(self):
        self.assertEqual(getMemory(os.path.join(self.path_to_testing, "ma.c")), 0)
