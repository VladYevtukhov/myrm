# MYRM #

Alternative to the standard trash of the operating system, the settings of which can be changed through a config file or set at each separate start, as well as the recording of the results of work in the log file.

## List of myrm operations ##

* Delete file / directory to trash
* Delete objects by regular expression in the specified directory to trash
* View trash
* Manual cleaning of the trash
* Automatic cleaning by different policies
* Restore objects from the trash

## What is this repository for? ##

Repository allows you to learn, download and use the myrm utility

## How do I get set up? ##

	Сlone or download this repository
	Go to the local repository
	Write "python setup.py install --user" to install utility
	Write "myrm -h" for a list of utility options

## Who do I talk to? ##

	Vlad Yevtukhov
	nyti96@yandex.ru