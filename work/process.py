"""Process module.

The module performs preliminary processing of all operations.

Example:
    process.deleteFile(file_name, trash_file_path, trash_info_path, confirmation, max_trash_memory, trash_file_size,
               silent_mode, path_to_trash, dry_run)
"""
from work import service
from work import politics
from work import bin
import os
import re


def deleteFile(file_name, trash_file_path, trash_info_path, confirmation, max_trash_memory, trash_file_size,
               silent_mode, path_to_trash, dry_run, lock=None):
    occupied_trash_memory = politics.getMemory(trash_file_path)
    occupied_file_memory = politics.getMemory(file_name)
    if max_trash_memory - occupied_trash_memory > occupied_file_memory:
        if politics.trashSize(trash_file_size, trash_info_path):
            if confirmation and not silent_mode:
                if not os.path.exists(file_name):
                    service.silentPrint("File not found", "File \"{}\" not found".format(file_name), silent_mode,
                                        dry_run, path_to_trash, lock)
                    raise OSError(2)
                question = raw_input("You really want to delete file?(y/n): ")
                if question == 'y':
                    bin.deleteForTrash(file_name, trash_file_path, trash_info_path, path_to_trash, dry_run,
                                       silent_mode)
            elif not confirmation and not silent_mode:
                if not os.path.exists(file_name):
                    service.silentPrint("File not found", "File \"{}\" not found".format(file_name), silent_mode,
                                        dry_run, path_to_trash, lock)
                    raise OSError(2)
                bin.deleteForTrash(file_name, trash_file_path, trash_info_path, path_to_trash, dry_run,
                                   silent_mode)
            else:
                bin.deleteForTrash(file_name, trash_file_path, trash_info_path, path_to_trash, dry_run,
                                   silent_mode)
        else:
            if confirmation and not silent_mode:
                if not os.path.exists(file_name):
                    service.silentPrint("File not found", "File \"{}\" not found".format(file_name), silent_mode,
                                        dry_run, path_to_trash, lock)
                    raise OSError(2)
                question = raw_input("Trash is full, delete file permanently?(y/n): ")
                if question == "y":
                    service.eraseFile(file_name, dry_run)
            else:
                if not os.path.exists(file_name):
                    raise OSError(2)
                service.eraseFile(file_name, dry_run)
                name = os.path.basename(file_name)
                service.silentPrint("sorry man but \"{}\" file has been destroyed".format(name),
                                    "File \"{}\" has been destroyed".format(name), silent_mode, dry_run, path_to_trash,
                                    lock)
    else:
        if confirmation and not silent_mode:
            question = raw_input("Not enough memory in trash, delete permanently?(y/n): ")
            if question == "y":
                service.eraseFile(file_name, dry_run)
        else:
            service.eraseFile(file_name, dry_run)
            name = os.path.basename(file_name)
            service.silentPrint("sorry man but \"{}\" file has been destroyed".format(name),
                                "File \"{}\" has been destroyed".format(name), silent_mode, dry_run, path_to_trash, lock)


def processingForClear(file_name, confirmation, trash_file_path, trash_info_path, dry_run, silent_mode, lock=None):
    path_to_trash = os.path.dirname(os.path.dirname(trash_file_path)) + '/'
    if confirmation and not silent_mode:
        if not os.path.exists(os.path.join(trash_file_path, file_name)):
            service.silentPrint("File not found", "File \"{}\" not found".format(file_name), silent_mode, dry_run,
                                path_to_trash, lock)
            raise OSError(2)
        question = raw_input("You really want to erase file?(y/n): ")
        if question == 'y':
            bin.clearFile(file_name, trash_file_path, trash_info_path, dry_run, silent_mode)
    else:
        bin.clearFile(file_name, trash_file_path, trash_info_path, dry_run, silent_mode)


def processingForClearAll(trash_info_path, trash_file_path, confirmation, dry_run, silent_mode, lock=None):
    total = 0
    path_to_trash = os.path.dirname(os.path.dirname(trash_file_path)) + '/'
    if confirmation and not silent_mode:
        if len(os.listdir(trash_info_path)) == 0:
            service.silentPrint("Files not found", "File not found", silent_mode, dry_run, path_to_trash, lock)
            return
        question = raw_input("You really want to erase everything?(y/n): ")
        if question == 'y':
            total = bin.clearAllFile(trash_file_path, trash_info_path, dry_run, silent_mode)
    else:
        total = bin.clearAllFile(trash_file_path, trash_info_path, dry_run, silent_mode)
    return total


def processingForDelete(trash_file_path, trash_info_path, confirmation, max_trash_memory, trash_file_size,
                        silent_mode, path_to_trash, dry_run, path):
    file_name = os.path.join(os.getcwd(), path)
    deleteFile(file_name, trash_file_path, trash_info_path, confirmation, max_trash_memory,
               trash_file_size, silent_mode, path_to_trash, dry_run)


def processingForRegular(path, regular, trash_file_path, trash_info_path, confirmation, max_trash_memory,
                         trash_file_size, silent_mode, path_to_trash, dry_run, path_to_project, stat):
    file_for_del = os.listdir(path)
    total = 0
    for files in file_for_del:
        if re.search(regular, files):
            try:
                total += 1
                deleteFile(os.path.join(path, files), trash_file_path, trash_info_path, confirmation, max_trash_memory,
                           trash_file_size, silent_mode, path_to_trash, dry_run)
            except OSError as e:
                stat.exFiles(e.args[0])
        else:
            if os.path.isdir(os.path.join(path, files)):
                total += processingForRegular(os.path.join(path, files), regular, trash_file_path, trash_info_path,
                                              confirmation, max_trash_memory, trash_file_size, silent_mode,
                                              path_to_trash, dry_run, path_to_project, stat)
    return total


def processingForRecovery(name, trash_file_path, trash_info_path, force_overwrite, confirmation, dry_run, silent_mode,
                          lock=None):
    path_to_trash = os.path.dirname(os.path.dirname(trash_file_path)) + '/'
    if confirmation and not silent_mode:
        if not os.path.exists(os.path.join(trash_file_path, name)):
            service.silentPrint("File not found", "File \"{}\" not found".format(name), silent_mode, dry_run,
                                path_to_trash, lock)
            raise OSError(2)
        question = raw_input("You really want to recover file?(y/n): ")
        if question == 'y':
            bin.recoveryFile(name, trash_file_path, trash_info_path, force_overwrite, dry_run, silent_mode)
    else:
        bin.recoveryFile(name, trash_file_path, trash_info_path, force_overwrite, dry_run, silent_mode)
