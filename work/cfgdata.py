"""Cfgdata module.

Load config information.
"""
import os
import json
import logging
from work import service


class Cfgdata(object):
    """Cfgdata class.

    The class responsible for processing the configuration information
    """
    def __init__(self, args):
        config = self.loadConfigFile(args)
        self.path_to_project = config["path_to_project"]
        if not os.path.exists("{}/files/".format(self.path_to_project)):
            os.makedirs("{}/files/".format(self.path_to_project))
        logging.basicConfig(filename="{}/files/logfile.log".format(self.path_to_project), level=logging.DEBUG,
                            format='%(asctime)s: %(name)s: %(message)s')
        logging.debug("Try to load all arguments ")
        self.path_to_trash = config["path_to_trash"]
        if type(config["max_memory"]) is not int:
            self.max_trash_memory = int(config["max_memory"])
        else:
            self.max_trash_memory = config["max_memory"]
        if type(config["enable_confirmation"]) is not bool:
            self.confirmation = service.strToBool(config["enable_confirmation"])
        else:
            self.confirmation = config["enable_confirmation"]
        if type(config["enable_dry_run"]) is not bool:
            self.dry_run = service.strToBool(config["enable_dry_run"])
        else:
            self.dry_run = config["enable_dry_run"]
        if type(config["enable_force_overwrite"]) is not bool:
            self.force_overwrite = service.strToBool(config["enable_force_overwrite"])
        else:
            self.force_overwrite = config["enable_force_overwrite"]
        if type(config["time_until_auto_erase"]) is not int:
            self.time_until_auto_erase = int(config["time_until_auto_erase"])
        else:
            self.time_until_auto_erase = config["time_until_auto_erase"]
        if type(config["trash_file_max_size"]) is not int:
            self.trash_file_size = int(config["trash_file_max_size"])
        else:
            self.trash_file_size = config["trash_file_max_size"]
        if type(config["records_per_page"]) is not int:
            self.records_per_page = int(config["records_per_page"])
        else:
            self.records_per_page = config["records_per_page"]
        if type(config["enable_silent_mode"]) is not bool:
            self.silent_mode = service.strToBool(config["enable_silent_mode"])
        else:
            self.silent_mode = config["enable_silent_mode"]
        logging.debug("Arguments load successfully")
        self.trash_file_path = os.path.join(config["path_to_trash"], "files/")
        self.trash_info_path = os.path.join(config["path_to_trash"], "info/")

    def loadConfigFile(self, args):
        """loadConfigFile method.

        Load config file.
        """
        if os.path.exists(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "config.json")):
            with open(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "config.json"), 'r') as cfg:
                config = json.load(cfg)
        else:
            with open(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "config.json"), 'a+') as cfg:
                config = self.loadConfigData()
                json.dump(config, cfg)
        with open(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "human_config"), 'a+') as human_cfg:
            lines = human_cfg.read().splitlines()
            for line in lines:
                key_value = line.split(' ')
                config[key_value[0]] = key_value[1]
        config = self.userArgs(config, args)
        return config

    @staticmethod
    def loadConfigData():
        config = {"path_to_trash": "/home/vlad/Desktop/trash/",
                  "max_memory": 5000000,
                  "enable_confirmation": True,
                  "enable_dry_run": False,
                  "enable_force_overwrite": False,
                  "time_until_auto_erase": 30000000,
                  "trash_file_max_size": 15,
                  "records_per_page": 5,
                  "enable_silent_mode": False,
                  "path_to_project": os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
                  }
        return config

    @staticmethod
    def userArgs(cfg, arg):
        """userArgs staticMethod.

        Load custom arguments.
        """
        if arg.max_memory:
            cfg["max_memory"] = arg.max_memory
        if arg.enable_confirmation:
            cfg["enable_confirmation"] = arg.enable_confirmation
        if arg.enable_dry_run:
            cfg["enable_dry_run"] = arg.enable_dry_run
        if arg.enable_force_overwrite:
            cfg["enable_force_overwrite"] = arg.enable_force_overwrite
        if arg.time_until_auto_erase:
            cfg["time_until_auto_erase"] = arg.time_until_auto_erase
        if arg.trash_file_max_size:
            cfg["trash_file_max_size"] = arg.trash_file_max_size
        if arg.records_per_page:
            cfg["records_per_page"] = arg.records_per_page
        if arg.silent_mode:
            cfg["enable_silent_mode"] = arg.silent_mode
        return cfg
