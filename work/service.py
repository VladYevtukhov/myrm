"""Service module.

Secondary functions.

Example:
    service.checkCurrentFile(name, path)
"""
import os
import shutil
import logging


def checkCurrentFile(name, path):
    """checkCurrentFile function.

    Verifying the name on plagiarism.

    Parameters:
        name: started file name.
        path: path to trash.

    Return: final file name.
    """
    index = 0
    fileWithTrash = os.listdir(path)
    while True:
        uniquenessCheck = True
        if os.path.exists(os.path.join(path,name)) and index == 0:
            index += 1
            uniquenessCheck = False
        elif os.path.exists(os.path.join(path,name)+ '.' + str(index)):
            index += 1
            uniquenessCheck = False
        if uniquenessCheck:
            if index > 0:
                return name + '.' + index.__str__()
            else:
                return name


def move(name, path, new_path, dry_run):
    if not os.path.exists(path):
        raise OSError(2)
    elif not os.access(path, os.W_OK):
        raise OSError(13)
    if not dry_run:
        temp = checkCurrentFile("temp", os.path.dirname(os.path.dirname(__file__)))
        os.mkdir("{}{}/".format(os.path.dirname(os.path.dirname(__file__)), temp))
        shutil.move(path, "{}{}".format(os.path.dirname(os.path.dirname(__file__)), temp))
        info = checkCurrentFile(name, new_path)
        lastName = os.path.basename(path)
        os.rename("{}{}/{}".format(os.path.dirname(os.path.dirname(__file__)), temp, lastName),
                  "{}{}/{}".format(os.path.dirname(os.path.dirname(__file__)), temp, info))
        shutil.move("{}{}/{}".format(os.path.dirname(os.path.dirname(__file__)), temp, info), new_path)
        shutil.rmtree("{}{}".format(os.path.dirname(os.path.dirname(__file__)), temp))


def eraseFile(path, dry_run):
    if not os.path.exists(path):
        raise OSError(2)
    elif not os.access(path, os.W_OK):
        raise OSError(13)
    if not dry_run:
        if os.path.islink(path):
            os.unlink(path)
        elif os.path.isdir(path):
            shutil.rmtree(path)
        else:
            os.remove(path)


def makeTrash(path_to_trash):
    if os.path.exists(path_to_trash):
        if not os.path.exists(os.path.join(path_to_trash, "files/")):
            os.makedirs(os.path.join(path_to_trash, "files/"))
        if not os.path.exists(os.path.join(path_to_trash, "info/")):
            os.makedirs(os.path.join(path_to_trash, "info/"))
    else:
        os.makedirs(os.path.join(path_to_trash, "files/"))
        os.makedirs(os.path.join(path_to_trash, "info/"))


def strToBool(text):
    if text == "True":
        return True
    elif text == "False":
        return False


def silentPrint(text, log, silent_mode, dry_run, path_to_trash, lock):
    if not silent_mode and len(text) > 0:
        print text
    logger = logging.getLogger(path_to_trash)
    if lock is not None:
        with lock:
            if not dry_run:
                logger.debug(log)
            else:
                logger.debug("{} (with dry_run)".format(log))
    else:
        if not dry_run:
            logger.debug(log)
        else:
            logger.debug("{} (with dry_run)".format(log))
