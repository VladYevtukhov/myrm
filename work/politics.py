"""Politics module.

Cleaning policy.

Example:
    politics.getMemory(path)
"""
import os
from datetime import datetime
from work import bin
import logging


def getMemory(path):
    """getMemory function.

    Get the size of the object.

    Return: Object size.
    """
    sub_size = 0
    if os.path.isfile(path):
        return os.path.getsize(path)
    for dirpath, dirnames, filenames in os.walk(path):
        for filename in filenames:
            fp = os.path.join(dirpath, filename)
            if os.path.exists(fp):
                sub_size += os.path.getsize(fp)
    return sub_size


def autoErase(reset_time, trash_file_path, trash_info_path, dry_run, silent_mode):
    """autoErase function.

    Erase files by time.

    Parameters:
        reset_time: time before erase file.
        trash_file_path: path to contents of trash.
        trash_info_path: path to storing information about trash files.
        dry_run: dry run mode.
        silent_mode: silent mode.
    """
    logging.debug("Check for auto erase files in trash")
    file_list = os.listdir(trash_info_path)
    for files in file_list:
        text = open(trash_info_path + files, 'r')
        line = text.read().splitlines()
        timestamp = (datetime.now() - datetime.strptime(line[2], '%Y-%m-%d %H:%M:%S.%f')).total_seconds() * 1000
        if timestamp >= reset_time:
            files = files[:-10]
            bin.clearFile(files, trash_file_path, trash_info_path, dry_run, silent_mode)


def trashSize(trash_file_max_size, trash_info_path):
    if not os.path.exists(trash_info_path):
        return False
    fileWithTrash = os.listdir(trash_info_path)
    if trash_file_max_size > len(fileWithTrash):
        return True
    else:
        return False
