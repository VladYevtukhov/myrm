"""Bin module.

The module processes all the basic operations.

Example:
    bin.trashWatch(path, records_per_page)
"""
import os
import errno
from datetime import datetime
from work import service


def trashWatch(path, records_per_page):
    """trashWatch function.

    Processes the contents of the trash.

    Parameters:
        path: path to trash.
        records_per_page: limit the number of output files.

    Return: contents list.
    """
    index = 0
    files = []
    for name in os.listdir(path):
        files.append(name)
        index += 1
        if index == records_per_page:
            index = 0
            down = raw_input("next records?(y/n): ")
            if down == 'y':
                continue
            else:
                break
    for name in files:
        print name
    return files


def deleteForTrash(file_name, trash_file_path, trash_info_path, path_to_trash, dry_run, silent_mode, lock=None):
    """deleteForTrash function.

    Processes file deletion to trash.

    Parameters:
        file_name: path to file.
        trash_file_path: path to contents of trash.
        trash_info_path: path to storing information about trash files.
        path_to_trash: path to trash
        dry_run: dry run mode.
        silent_mode: silent mode.
    """
    service.silentPrint("", ("Try to delete \"{}\" for trash".format(file_name)), silent_mode, dry_run, path_to_trash,
                        lock)
    trashes = (trash_file_path, path_to_trash, trash_info_path)
    if (file_name + '/' in trashes) or (file_name in trashes):
        service.silentPrint("NO!", "Attempt to remove trash", silent_mode, dry_run, path_to_trash, lock)
        raise OSError(1)
    name = os.path.basename(file_name)
    try:
        name = service.checkCurrentFile(name, trash_file_path)
        service.move(name, file_name, trash_file_path, dry_run)
        if not dry_run:
            trashFileName = open("{}.trashinfo".format(trash_info_path + name), 'w')
            trashFileName.writelines(["[Trash Info]\n", "{}\n".format(file_name), str(datetime.now())])
            trashFileName.close()
        service.silentPrint("\"{}\" deleted successfully".format(name),
                            ("Delete \"{}\" for trash has been successfully".format(file_name)), silent_mode,
                            dry_run, path_to_trash, lock)
    except OSError as e:
        if e.args[0] == errno.EACCES:
            service.silentPrint("Denied. System file or directory",
                                "Try to delete system file or directory", silent_mode, dry_run, path_to_trash, lock)
            raise
        service.silentPrint("",("Delete \"{}\" for trash has been failed".format(file_name)),
                            silent_mode, dry_run, path_to_trash, lock)
        raise


def clearFile(file_name, trash_file_path, trash_info_path, dry_run, silent_mode, lock=None):
    """clearFile function.

    Processes file cleanup from trash.

    Parameters:
        file_name: file name.
        trash_file_path: path to contents of trash.
        trash_info_path: path to storing information about trash files.
        dry_run: dry run mode.
        silent_mode: silent mode.
    """
    path_to_trash = os.path.dirname(os.path.dirname(trash_file_path)) + '/'
    service.silentPrint("", ("Try to erase \"{}\" of trash".format(file_name)), silent_mode, dry_run, path_to_trash, lock)
    try:
        service.eraseFile(os.path.join(trash_file_path, file_name), dry_run)
        if not dry_run:
            os.remove("{}.trashinfo".format(trash_info_path + file_name))
        service.silentPrint("\"{}\" erased successfully".format(file_name),
                            ("Erase \"{}\" of trash has been successfully".format(file_name)), silent_mode,
                            dry_run, path_to_trash, lock)
    except OSError as e:
        if e.args[0] == errno.EACCES:
            service.silentPrint("Denied. System file or directory",
                                "Try to erase system file or directory", silent_mode, dry_run, path_to_trash, lock)
            raise
        service.silentPrint("File not found", ("Erase \"{}\" of trash has been failed".format(file_name)), silent_mode,
                            dry_run, path_to_trash, lock)
        raise


def clearAllFile(trash_file_path, trash_info_path, dry_run, silent_mode):
    """clearAllFile function.

    Processes a list of files for cleaning out of trash.

    Parameters:
        trash_file_path: path to contents of trash.
        trash_info_path: path to storing information about trash files.
        dry_run: dry run mode.
        silent_mode: silent mode.

    Return: exception count.
    """
    allTrashFile = os.listdir(trash_file_path)
    total = 0
    for filename in allTrashFile:
        try:
            clearFile(filename, trash_file_path, trash_info_path, dry_run, silent_mode)
        except OSError as e:
            if e.args[0] == errno.EACCES:
                total += 1
    return total


def recoveryFile(name, trash_file_path, trash_info_path, force_overwrite, dry_run, silent_mode, lock=None):
    """recoveryFile function.

    Processes file recovery from trash.

    Parameters:
        name: file name.
        trash_file_path: path to contents of trash.
        trash_info_path: path to storing information about trash files.
        force_overwrite: overwrite mode.
        dry_run: dry run mode.
        silent_mode: silent mode.
    """
    line = None
    path_to_trash = os.path.dirname(os.path.dirname(trash_file_path)) + '/'
    service.silentPrint("", ("Try to recovery \"{}\" of trash".format(name)), silent_mode, dry_run, path_to_trash, lock)
    try:
        with open("{}.trashinfo".format(trash_info_path + name), 'r') as filename:
            line = filename.read().splitlines()
    except IOError as e:
        if e.args[0] == errno.ENOENT:
            service.silentPrint("File not found", ("\"{}.trashinfo\" not found in trash".format(name)), silent_mode,
                                dry_run, path_to_trash, lock)
            raise OSError(2)
    newPath = os.path.dirname(line[1])
    origin_name = os.path.basename(line[1])
    if not os.path.exists(newPath):
        os.makedirs(newPath)
    if force_overwrite:
        if os.path.exists(line[1]):
            service.eraseFile(line[1], dry_run)
    try:
        service.move(origin_name, trash_file_path + name, newPath, dry_run)
        if not dry_run:
            os.remove("{}.trashinfo".format(trash_info_path + name))
        service.silentPrint("\"{}\" recovered successfully".format(origin_name),
                            ("Recovery \"{}\" of trash has been successfully".format(name)), silent_mode, dry_run,
                            path_to_trash, lock)
    except OSError as e:
        if e.args[0] == errno.ENOENT:
            service.silentPrint("File not found", ("Recovery \"{}\" of trash has been failed".format(name)),
                                silent_mode, dry_run, path_to_trash, lock)
            raise
        elif e.args[0] == errno.EACCES:
            service.silentPrint("Denied. System file or directory",
                                "Recovery \"{}\" of trash has been failed".format(name), silent_mode, dry_run,
                                path_to_trash, lock)
            raise
