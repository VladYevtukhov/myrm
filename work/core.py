import os
from work import cfgdata
from work import politics
from work import statistics
from work import bin
from work import service
from work import parser
from work import process


def coreFunc():
    args = parser.parseArgument()
    config = cfgdata.Cfgdata(args)
    trash_file_path = config.trash_file_path
    trash_info_path = config.trash_info_path
    stat = statistics.Statictics(config.path_to_project)
    service.makeTrash(config.path_to_trash)
    politics.autoErase(config.time_until_auto_erase, trash_file_path, trash_info_path, config.dry_run,
                       config.silent_mode)
    if args.clear:
        for file_name in args.clear:
            try:
                process.processingForClear(file_name, config.confirmation, trash_file_path, trash_info_path,
                                           config.dry_run, config.silent_mode)
            except OSError as e:
                stat.exFiles(e.args[0])
        if not config.dry_run:
            stat.writeToFile("Total {} files. Clear {} files.".format(len(args.clear), len(args.clear) - stat.all_files))
    if args.clearAll:
        if len(os.listdir(trash_info_path)) == 0:
            print "Trash is clear"
            return
        total = len(os.listdir(trash_info_path))
        exc = 0
        try:
            exc = process.processingForClearAll(trash_info_path, trash_file_path, config.confirmation, config.dry_run,
                                                config.silent_mode)
        except OSError as e:
            stat.exFiles(e.args[0])
        if not config.dry_run:
            stat.system_file_index = exc
            stat.writeToFile("Total {} files. Clear {} files.".format(total, total - exc))
    if args.delete:
        for path in args.delete:
            try:
                process.processingForDelete(trash_file_path, trash_info_path, config.confirmation,
                                            config.max_trash_memory, config.trash_file_size, config.silent_mode,
                                            config.path_to_trash, config.dry_run, path)
            except OSError as e:
                stat.exFiles(e.args[0])
        if not config.dry_run:
            stat.writeToFile("Total {} files. Deleted {} files.".format(len(args.delete),
                                                                       len(args.delete) - stat.all_files))
    if args.watch:
        bin.trashWatch(trash_file_path, config.records_per_page)
    if args.regular:
        name = args.regular
        pathForRegular = os.getcwd()
        total = process.processingForRegular(pathForRegular, name, trash_file_path, trash_info_path,
                                             config.confirmation, config.max_trash_memory, config.trash_file_size,
                                             config.silent_mode, config.path_to_trash, config.dry_run,
                                             config.path_to_project, stat)
        if not config.dry_run:
            stat.writeToFile("Total {} files. Deleted {} files.".format(total, total - stat.all_files))
    if args.recovery:
        for name in args.recovery:
            try:
                process.processingForRecovery(name, trash_file_path, trash_info_path, config.force_overwrite,
                                              config.confirmation, config.dry_run, config.silent_mode)
            except OSError as e:
                stat.exFiles(e.args[0])
        if not config.dry_run:
            stat.writeToFile("Total {} files. Recovered {} files.".format(len(args.recovery),
                                                                         len(args.recovery) - stat.all_files))
    stat.closeFile()
