import argparse


def parseArgument():
    pars = argparse.ArgumentParser()
    pars.add_argument("-d", "--delete", help="Delete file by name", action="store", nargs='+', default=[])
    pars.add_argument("-w", "--watch", help="View all file in the trash", action="store_true")
    pars.add_argument("-c", "--clear", help="Clear file from trash by name", action="store", nargs='+', default=[])
    pars.add_argument("-ca", "--clearAll", help="Clear trash", action="store_true")
    pars.add_argument("-dr", "--regular", help="Delete files by regular expression", action="store", type=str)
    pars.add_argument("-r", "--recovery", help="Restore file by name", action="store", nargs='+', default=[])
    pars.add_argument("-mm", "--max_memory", help="Change max memory for trash", action="store", type=int)
    pars.add_argument("-enc", "--enable_confirmation", help="Change confirmation", action="store", type=str)
    pars.add_argument("-edr", "--enable_dry_run", help="On\Off dry_run mode", action="store", type=str)
    pars.add_argument("-efo", "--enable_force_overwrite", help="Use True for overwrite file names",
                      action="store", type=str)
    pars.add_argument("-t", "--time_until_auto_erase", help="Time until auto erase in trash", action="store", type=int)
    pars.add_argument("-ts", "--trash_file_max_size", help="Max size in trash for files", action="store", type=int)
    pars.add_argument("-rp", "--records_per_page", help="Records per page", action="store", type=int)
    pars.add_argument("-esm", "--silent_mode", help="On\Off silent mode", action="store", type=str)
    return pars.parse_args()
