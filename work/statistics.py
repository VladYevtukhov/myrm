import os
import errno


class Statictics(object):

    def __init__(self, path_to_project):
        self.temp = open(os.path.join(path_to_project, "files/statistics"), 'a+')
        self.all_files = 0
        self.file_not_found_index = 0
        self.system_file_index = 0
        self.not_permitted_index = 0

    def closeFile(self):
        self.temp.close()

    def writeToFile(self, text):
        self.temp.write("{} Not found {} files, {} system files, not permitted {}.\n".format(text,
                                self.file_not_found_index, self.system_file_index, self.not_permitted_index))

    def exFiles(self, argument):
        if argument == errno.ENOENT:
            self.file_not_found_index += 1
        elif argument == errno.EACCES:
            self.system_file_index += 1
        elif argument == errno.EPERM:
            self.not_permitted_index += 1
        else:
            print "OSError[{}]".format(argument)
        self.all_files += 1
