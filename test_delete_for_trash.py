from unittest import TestCase
from work.bin import deleteForTrash
from work.bin import clearAllFile
from work.bin import clearFile
from work.bin import recoveryFile
import tempfile
import os
import shutil


class CoreFunctionally(TestCase):
    def setUp(self):
        self.path_to_testing = tempfile.mkdtemp()
        self.test_trash_path = os.path.join(self.path_to_testing, "test_trash/")
        self.test_info_path = os.path.join(self.path_to_testing, "test_trash/info/")
        self.test_files_path = os.path.join(self.path_to_testing, "test_trash/files/")
        self.test_path = os.path.join(self.path_to_testing, "test")
        if not os.path.exists(self.test_files_path):
            os.makedirs(self.test_files_path)
        if not os.path.exists(self.test_info_path):
            os.makedirs(self.test_info_path)
        if not os.path.exists(self.test_path):
            with open(self.test_path, 'w'):
                pass
        os.chmod(self.test_path, 0777)

    def tearDown(self):
        shutil.rmtree(self.path_to_testing)

    def test_deleteForTrash(self):
        deleteForTrash(self.test_path, self.test_files_path, self.test_info_path, self.test_trash_path, False, False)
        self.assertFalse(os.path.exists(self.test_path))

    def test_deleteForTrash2(self):
        deleteForTrash(self.test_path, self.test_files_path, self.test_info_path,
                       self.test_trash_path, False, False)
        try:
            deleteForTrash(self.test_path, self.test_files_path, self.test_info_path, self.test_trash_path,
                           False, False)
            self.fail()
        except OSError as e:
            self.assertEqual(e.args[0], 2)

    def test_deleteForTrash3(self):
        try:
            deleteForTrash(self.test_trash_path, self.test_files_path, self.test_info_path, self.test_trash_path,
                           False, False)
            self.fail()
        except OSError as e:
            self.assertEqual(e.args[0], 1)

    def test_fclearFile(self):
        deleteForTrash(self.test_path, self.test_files_path, self.test_info_path, self.test_trash_path, False, False)
        temp = clearFile("test", self.test_files_path, self.test_info_path, False, False)
        self.assertRaises(not OSError, temp)

    def test_fclearFile2(self):
        try:
            clearFile("test123", self.test_files_path, self.test_info_path, False, False)
            self.fail()
        except OSError as e:
            self.assertEqual(e.args[0], 2)

    def test_recovery(self):
        deleteForTrash(self.test_path, self.test_files_path, self.test_info_path, self.test_trash_path, False, False)
        temp = recoveryFile("test", self.test_files_path, self.test_info_path, False, False, False)
        self.assertRaises(not OSError, temp)

    def test_recovery2(self):
        try:
            recoveryFile("test.1", self.test_files_path, self.test_info_path, False, False, False)
            self.fail()
        except OSError as e:
            self.assertEqual(e.args[0], 2)

    def test_xclearAllFile(self):
        clearAllFile(self.test_files_path, self.test_info_path, False, False)
        self.assertEqual(len(os.listdir(self.test_files_path)), 0)
